import 'package:flutter/material.dart';
class ColorPallete{
  var brown = Color(0xff85714D);//Кричневый для кнопок и иконок AppBar;
  var accsentRed = Color(0xffFF4444);//Для текста внизу главного экрана
  var backgroundBlack = Color(0xff151920);//Бэк для всех экранов
  var white = Colors.white;//Белый
  var gray = Color.fromRGBO(255, 255, 255, 0.6);//Для subtitle creditItem на экране CreditScreen
  var red = Color(0xffCF0016);// Для нижнего отступа на экране CreditScreen
  var lightBlack = Color(0xff232528);
  var errorBorder = Color(0XFFF64B63);//Для полей ввода главного экрана
  var appBarGradientBegin = Color(0xff2B2C32);// Градиент AppBar начальный цвет
  var appBarGradientEnd = Color(0xff21222C);// Градиент AppBar конечный цвет
  var yellow = Color(0xffFFB929); //Title в слайдере в CreditScreen,иконци в BottomNavigationBar
  var itemGradientBegin = Color.fromRGBO(40, 42, 44, 0);//градиент для item на экране CreditScreen начальный цвет
  var itemGradientEnd = Color.fromRGBO(17, 20, 22, 0.96);//градиент для item на экране CreditScreen конечный цвет
  var mySeparator = Color.fromRGBO(182, 188, 201, 0.5);// Цвет пунктирной линии
  var appBarIconTitle = Color(0xff666666); //цвет текста под иконкой appBar
  var bottomNavBarFirst = Color(0xff2B2C32);
  var bottomNavBarSecond = Color(0xff21222C);
  var colorDivider = Color.fromRGBO(224, 229, 231, 0.15);
  var bloodRed = Color(0xffC83A39);
  var lightRed = Color(0xffC83A39);
  var TextFieldInput = Color.fromRGBO(255, 255, 255, 0.87);
}