

import 'dart:ui' as prefix0;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:motors/Colors/ColorsPallete.dart';
import 'package:motors/pages/NavigatorScreen.dart';

void main()=>runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MainScreen(),
    )
);

class MainScreen extends StatelessWidget {
  final _formkey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context){
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/login_bg.png'), fit: BoxFit.cover),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: SingleChildScrollView(
            child:
            Container(
              height: MediaQuery.of(context).size.height,
              child: Padding(
                padding: EdgeInsets.only(left: 16.0, right: 17.0, bottom: 43.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 126.0),
                      child: Container(
                        child: SvgPicture.asset('assets/svg_icons/logo.svg'),
                      ),
                    ),
                    Container(
                      child: Form(
                        key: _formkey,
                        child: Column(
                          children: <Widget>[
                            TextField(
                              style: TextStyle(
                                color: ColorPallete().white, fontSize: 14.0,),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(top: 10.0,
                                  bottom: 10.0,
                                  left: 17.0,
                                ),
                                hintText: 'Логин',
                                hintStyle: TextStyle(fontSize: 14.0,
                                  color: Colors.white,),
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                    borderSide: BorderSide(
                                        color: ColorPallete().white)
                                ),
                                errorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide(
                                      color: ColorPallete().errorBorder),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide(
                                    color: ColorPallete().white,
                                    width: 1.5,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 19.0,),
                            TextFormField(
                              style: TextStyle(
                                color: ColorPallete().white, fontSize: 14.0,),
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(top: 10.0,
                                    bottom: 10.0,
                                    left: 17.0,
                                  ),
                                  hintText: 'Пароль',
                                  hintStyle: TextStyle(fontSize: 14.0,
                                      color: ColorPallete().white,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0.5),
                                  hasFloatingPlaceholder: false,
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      borderSide: BorderSide(
                                          color: ColorPallete().white)
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                    borderSide: BorderSide(
                                      color: ColorPallete().white,
                                      width: 1.5,
                                    ),
                                  )
                              ),
                            ),
                            SizedBox(height: 21.0,),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: FlatButton(
                                    onPressed: () {
                                      Navigator.push(context, MaterialPageRoute(
                                          builder: (context) => StartScreen()));
                                    },
                                    color: ColorPallete().brown,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10.0)),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 15.0),
                                      child: Text('ВОЙТИ',
                                        style: TextStyle(
                                            color: ColorPallete().white),),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          ClipRect(
                            child: BackdropFilter(
                              filter: prefix0.ImageFilter.blur(
                                sigmaX: 3.0,
                                sigmaY: 3.0,
                              ),
                              child: GestureDetector(
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10.0),
                                    border: Border.all(color: ColorPallete().white,
                                      width: 1.0,
                                      style: BorderStyle.solid,
                                    ),
                                  ),
                                  child: Row(
                                    children: <Widget>[
                                      Padding(
                                          padding: const EdgeInsets.only(
                                              top: 9.0, left: 13, bottom: 8.0),
                                          child: Image.asset('images/google_plus.png')
                                      ),
                                      Expanded(
                                          child: Container(child: Text(
                                            'ВОЙТИ ЧЕРЕЗ GMAIL', style: TextStyle(
                                            color: Colors.white, fontSize: 14.0,),
                                            textAlign: TextAlign.center,))),
                                    ],
                                  ),
                                ),
                                onTap: null,
                              ),
                            ),
                          ),
                          SizedBox(height: 26.0,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Text('Забыли пароль?',
                                  style: TextStyle(color: ColorPallete().accsentRed,
                                    fontSize: 14.0,)),
                              Text('Регистрация',
                                  style: TextStyle(color: ColorPallete().accsentRed,
                                      fontSize: 14.0)),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}




