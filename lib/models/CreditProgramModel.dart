
class ProgrammList{
  final String leadingImage;
  final String title;
  final String subtitle;

  ProgrammList(this.leadingImage, this.title, this.subtitle);
}
