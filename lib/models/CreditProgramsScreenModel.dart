import 'package:flutter/material.dart';

class ProgramsItem{
  final String imageUrl;
  final String maxSum;
  final String minPercent;
  final String title;
  final Color programColor;
  final String description;


  ProgramsItem(this.title,this.maxSum,this.minPercent,this.imageUrl,this.programColor,this.description);
}