

class DrawerItems{
  final String image;
  final String title;

  DrawerItems(this.image, this.title);
}