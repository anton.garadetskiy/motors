

class PersonalArea{
  final String iconPath;
  final String title;
  final String subtitle;

  PersonalArea(this.iconPath,this.title,this.subtitle);
}