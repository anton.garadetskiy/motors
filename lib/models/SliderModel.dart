
class SliderItems{
  final String imageUrl;
  final String carModel;
  final String carPrice;

  SliderItems(this.imageUrl,this.carModel,this.carPrice);
}