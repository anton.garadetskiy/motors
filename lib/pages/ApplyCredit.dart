import 'package:flutter/material.dart';
import 'package:motors/Colors/ColorsPallete.dart';
import 'package:motors/models/TextFormFieldModel.dart';

class ApplyCredit extends StatefulWidget {
  @override
  _ApplyCreditState createState() => _ApplyCreditState();
}

class User{
  bool nameError = false;
  bool secondNameError = false;
  bool iinError = false;
  bool emailError = false;
  bool phoneError = false;
   String userName='';
   String userSecondName='';
   String userIin='';
   String userEmail='';
   String userPhone='';
}
User _user = User();

class _ApplyCreditState extends State<ApplyCredit> {
  List<textField> field =[textField(TextInputType.text,'Имя',_user.userName),
    textField(TextInputType.text,'Фамилия',_user.userSecondName),
    textField(TextInputType.number,'ИИН',_user.userIin),
    textField(TextInputType.emailAddress,'E-mail',_user.userEmail),
    textField(TextInputType.phone,'Телефон',_user.userPhone),];

  Widget textFormField(textField type,BuildContext context){
    return TextFormField(
      style: TextStyle(color: ColorPallete().TextFieldInput),
      keyboardType: type.keyBoardType,
      decoration: InputDecoration(
        labelText: type.LabelText,
        labelStyle: TextStyle(color: !_user.nameError?ColorPallete().brown:ColorPallete().white),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: BorderSide(
            color: !_user.nameError?ColorPallete().brown:ColorPallete().bloodRed,
          ),
        ),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: ColorPallete().brown,
            )
        ),
        errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: ColorPallete().bloodRed,
            )
        ),
      ),
      validator: (value){
        if(value.isEmpty) return '';
      },
      onSaved: (value)=>setState(()=>type.saveTo = value),
    );
  }
  final _formKey = GlobalKey<FormState>();
  bool _errorVisible = false;
  bool _agreement=false;

  void _errorVisibleTrue() {
    setState(() {
      _errorVisible = true;
    });
  }

  RegExp regExp = new RegExp(
    r"@^WS{1,2}:\/\/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:56789",
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:ColorPallete().backgroundBlack,
      body:  SingleChildScrollView(
        child:  Container(
          padding: EdgeInsets.only(top: 18.0,left: 16.0,right: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 18.0,),
              Center(
                  child: Text('ПОДАЧА ЗАЯВКИ НА КРЕДИТ “КЛАССИК”',style: TextStyle(fontSize: 14.0,color: ColorPallete().white),textAlign: TextAlign.center,)),
              SizedBox(height: 29.0,),
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    textFormField(field[0], context),
                    SizedBox(height: 25.0,),
                    textFormField(field[1], context),
                    SizedBox(height: 25.0,),
                    textFormField(field[2], context),
                    SizedBox(height: 25.0,),
                    textFormField(field[3], context),
                    SizedBox(height: 25.0,),
                    textFormField(field[4], context),
                  ],
                ),
              ),
              Visibility(
                visible: _errorVisible,
                  child: Text('Все поля обязательны для заполнения',style: TextStyle(fontSize: 12.0,color: ColorPallete().yellow))),
              SizedBox(height: 22.0,),
              Row(
                children: <Widget>[
                  Theme(data: Theme.of(context).copyWith(
                    unselectedWidgetColor: Color(0xff0D8D8D8),
                  ),
                    child: Checkbox(
                      activeColor: ColorPallete().backgroundBlack,
                      checkColor: Color(0xffD8D8D8),
                      onChanged: (bool value) => setState(() => _agreement = value),
                      value: _agreement,
                    ),
                  ),
                  Text('Cогласие сбора персональных данных.',style: TextStyle(fontSize: 14.0,color: Color(0xffA1A1A1)),)
                ],
              ),
              SizedBox(height: 38.0,),
              Row(
                children: <Widget>[
                  Expanded(
                    child: FlatButton(
                      onPressed: () {
                        if(_formKey.currentState.validate()){

                        }else{
                          _errorVisibleTrue();
                          _user.nameError = true;
                          _user.secondNameError = true;
                          _user.iinError = true;
                          _user.emailError = true;
                          _user.phoneError = true;
                        }
                      },
                      color: ColorPallete().brown,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 15.0),
                        child: Text('ПОДАТЬ ЗАЯВКУ НА КРЕДИТ',
                          style: TextStyle(color: ColorPallete().white),),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 33.0,),
            ],
          ),
        ),

      ),

    );
  }
}





