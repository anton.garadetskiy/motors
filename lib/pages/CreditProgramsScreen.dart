import 'package:flutter/material.dart';
import 'package:motors/Colors/ColorsPallete.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:motors/models/CreditProgramsScreenModel.dart';
import 'package:motors/pages/CreditProgramsScreenAbout.dart';



class CreditProgramItem extends StatelessWidget{

  CreditProgramItem({Key key}) : super(key: key);

  List<ProgramsItem> item = [ProgramsItem('Классик','20 000 000','10','https://article.images.consumerreports.org/f_auto/prod/content/dam/CRO%20Images%202018/Cars/December/CR-Cars-InlineHero-American-2018-Chevrolet-Corvette-GS-12-18', Color.fromRGBO(200, 58, 57, 0.85),'является наиболее распространенным способом приобретения автомобиля в кредит, когда погашение основного долга и вознаграждения производиться на ежемесячной основе'),
    ProgramsItem('Легкий','20 000 000','20','https://specials-images.forbesimg.com/imageserve/5d35eacaf1176b0008974b54/960x0.jpg?cropX1=790&cropX2=5350&cropY1=784&cropY2=3349',Color.fromRGBO(103, 113, 228, 0.85),'является наиболее распространенным способом приобретения автомобиля в кредит, когда погашение основного долга и вознаграждения производиться на ежемесячной основе'),
    ProgramsItem('Subaru','20 000 000','10','https://st3.zr.ru/_ah/img/W3Yc2cNyBopOkQ8MrIiF0g=s800',Color.fromRGBO(155, 155, 155, 0.85),'является наиболее распространенным способом приобретения автомобиля в кредит, когда погашение основного долга и вознаграждения производиться на ежемесячной основе'),
    ProgramsItem('Авто Б/У','20 000 000','10','https://robbreportedit.files.wordpress.com/2019/10/1959-porsche-356-outlaw-13.jpg?w=480',Color.fromRGBO(96, 147, 57, 0.85),'является наиболее распространенным способом приобретения автомобиля в кредит, когда погашение основного долга и вознаграждения производиться на ежемесячной основе')];

  Widget programItem(ProgramsItem program, BuildContext context){
    return GestureDetector(
      onTap: () =>
          Navigator.push(context, MaterialPageRoute(builder: (context)=>CreditProgramAbout(ProgramsItem('Классик','20 000 000','10','https://article.images.consumerreports.org/f_auto/prod/content/dam/CRO%20Images%202018/Cars/December/CR-Cars-InlineHero-American-2018-Chevrolet-Corvette-GS-12-18', Color.fromRGBO(200, 58, 57, 0.85),'является наиболее распространенным способом приобретения автомобиля в кредит, когда погашение основного долга и вознаграждения производиться на ежемесячной основе'),))
          ),
      child: Container(
        height: 165,
        margin: EdgeInsets.only(left: 16, bottom: 7, top: 7),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
          color: ColorPallete().red,
        ),
        child: Container(
          margin: EdgeInsets.only(left: 3),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
            color: ColorPallete().backgroundBlack,
          ),
          child: Stack(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: 134,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
                    image: DecorationImage(image: NetworkImage(program.imageUrl), fit: BoxFit.cover)
                ),
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: Container(
                    padding: EdgeInsets.only(top: 8, left: 11),
                    height: 72,
                    width: 116,
                    decoration: BoxDecoration(
                        color: program.programColor,
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(20))
                    ),
                    child: Align(
                      alignment: Alignment.topCenter,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(program.title, style: TextStyle(color: ColorPallete().white, fontSize: 14),),
                          SizedBox(width: 5,),
                          Icon(Icons.arrow_forward, color: ColorPallete().white, size: 18,)
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  width: double.infinity,
                  height: 65,
                  margin: EdgeInsets.only(right: 14),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10), topRight: Radius.circular(10), bottomRight: Radius.circular(10)),
                      color: ColorPallete().lightBlack),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Padding(padding: EdgeInsets.only(left: 13, right: 13),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text("МАКСИМАЛЬНАЯ СУММА МИКРОКРЕДИТА", style: TextStyle(color: ColorPallete().yellow, fontSize: 10), textAlign: TextAlign.center,),
                              SizedBox(height: 7,),
                              Text(program.maxSum, style: TextStyle(color: ColorPallete().white, fontSize: 16),),
                            ],
                          ),),
                      ),
                      Container(
                        height: double.infinity,
                        width: 1,
                        color: ColorPallete().colorDivider,
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(padding: EdgeInsets.only(left: 13, right: 13),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text("МИНИМАЛЬНЫЙ ПЕРВОНАЧАЛЬНЫЙ ВЗНОС", style: TextStyle(color: ColorPallete().yellow, fontSize: 10), textAlign: TextAlign.center,),
                              SizedBox(height: 7,),
                              Text("${program.minPercent}%", style: TextStyle(color: ColorPallete().white, fontSize: 16),),
                            ],
                          ),),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorPallete().backgroundBlack,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            programItem(item[0], context),
            programItem(item[1], context),
            programItem(item[2], context),
            programItem(item[3], context),
            SizedBox(height: 30,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: FlatButton(
                      onPressed: (){},
                      color: ColorPallete().brown,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 12.0,top: 15.0,bottom: 15.0),
                        child: Row(
                        children: <Widget>[
                            SvgPicture.asset('assets/svg_icons/calculator.svg'),
                            Expanded(child: Container(child: Text('КРЕДИТНЫЙ КАЛЬКУЛЯТОР',style: TextStyle(fontSize: 14.0,color: ColorPallete().white),textAlign: TextAlign.center,))),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 30,),
          ],
        ),
      ),
    );
  }
}



