import 'package:flutter/material.dart';
import 'package:motors/Colors/ColorsPallete.dart';
import 'package:motors/models/CreditProgramScreenAboutModel.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:motors/models/CreditProgramsScreenModel.dart';
import 'package:motors/pages/ApplyCredit.dart';


class CreditProgramAbout extends StatelessWidget {

  List<CreditAbout> list = [CreditAbout('assets/svg_icons/alert.svg','Максимальная сумма микрокредита (млн.тг.)'),
    CreditAbout('assets/svg_icons/alert.svg','Минимальный первоначальный взнос (%)'),
    CreditAbout('assets/svg_icons/alert.svg','Максимальный срок микрокредита (лет)'),
    CreditAbout('assets/svg_icons/check_circle.svg','Погашение основного долга и вознаграждения – ежемесячно;'),
    CreditAbout('assets/svg_icons/check_circle.svg','Комиссия за организацию микрокредита – 5% от суммы микрокредита;'),
    CreditAbout('assets/svg_icons/check_circle.svg','Досрочное погашение (полное / частичное) – без штрафных санкций;'),
    CreditAbout('assets/svg_icons/check_circle.svg','Залоговое обеспечение – приобретаемый автомобиль.'),
    CreditAbout('assets/svg_icons/check_circle.svg','Потребуются минимальные затраты на оплату первоначального взноса;'),
    CreditAbout('assets/svg_icons/check_circle.svg','По Вашему желанию, сумма комиссии может быть включена в сумму микрокредита;'),
    CreditAbout('assets/svg_icons/check_circle.svg','Предусмотрена возможность оформления права собственности на приобретаемый автомобиль на третье лицо;'),
    CreditAbout('assets/svg_icons/check_circle.svg','Не нужно искать дополнительные залоги, в залог может быть оформлен только приобретаемый автомобиль;'),
    CreditAbout('assets/svg_icons/check_circle.svg','Вы можете погасить кредит в любой момент досрочно (полностью или частично) без штрафных санкций; Предусмотрено страхование Каско.'),
  ];

  final ProgramsItem program;
  CreditProgramAbout (this.program);

  Widget ListAboutCreditProgram(CreditAbout list,BuildContext context){
    return ListTile(
      leading: SvgPicture.asset(list.iconPath),
        title: Text(list.title,style: TextStyle(fontSize: 14.0,color: ColorPallete().white),),
      );
}
  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: ColorPallete().backgroundBlack,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
        Container(
          height: 237.0,
          margin: EdgeInsets.only(left: 16, top: 7),
          child: Stack(
            children: <Widget>[
              Container(
                height: 165.0,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0),bottomLeft: Radius.circular(10.0)),
                    color: ColorPallete().lightRed
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 3.0),
                height: 165.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0),bottomLeft: Radius.circular(10.0)),
                  color: ColorPallete().backgroundBlack
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 134.0,
                  margin: EdgeInsets.only(left: 3.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0)),
                      image: DecorationImage(
                          image: NetworkImage(program.imageUrl),fit: BoxFit.cover
                      )
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  margin: EdgeInsets.only(left: 18.0),
                  height: 175.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(20.0)),
                    color: program.programColor,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(23.0, 17.0, 17.0, 7.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Программа «${program.title}»',style: TextStyle(fontSize: 16.0,color: ColorPallete().white,fontWeight: FontWeight.w700),),SizedBox(height: 10.0,),
                        Text(program.description,style: TextStyle(fontSize: 16.0,color: ColorPallete().white)),
                      ],
                    ),
                  ),
                ),
              )
            ],
          )
        ),
            SizedBox(height: 28.0,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                children: <Widget>[
                  Text('УСЛОВИЯ',style: TextStyle(fontSize: 14.0,color: ColorPallete().bloodRed),),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: Divider(color: ColorPallete().bloodRed,),
                  ),
                  ListAboutCreditProgram(list[0], context),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: Divider(color: ColorPallete().bloodRed,),
                  ),
                  ListAboutCreditProgram(list[1], context),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: Divider(color: ColorPallete().bloodRed,),
                  ),
                  ListAboutCreditProgram(list[2], context),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: Divider(color: ColorPallete().bloodRed,),
                  ),
                  SizedBox(height: 11.0),
                  ListAboutCreditProgram(list[3], context),
                  ListAboutCreditProgram(list[4], context),
                  ListAboutCreditProgram(list[5], context),
                  ListAboutCreditProgram(list[6], context),
                  SizedBox(height: 20.0,),
                  Text('ПРИЕМУЩЕСТВА:',style: TextStyle(fontSize: 14.0,color: ColorPallete().bloodRed),),
                  SizedBox(height: 13.0,),
                  ListAboutCreditProgram(list[7], context),
                  ListAboutCreditProgram(list[8], context),
                  ListAboutCreditProgram(list[9], context),
                  ListAboutCreditProgram(list[10], context),
                  ListAboutCreditProgram(list[11], context),
                  SizedBox(height: 23.0,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: FlatButton(
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>ApplyCredit()));
                          },
                          color: ColorPallete().brown,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 15.0),
                            child: Text('ПОДАТЬ ЗАЯВКУ НА КРЕДИТ',
                              style: TextStyle(color: ColorPallete().white),),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 19.0,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: OutlineButton(
                          borderSide: BorderSide(color: ColorPallete().brown,width: 1.0),
                          shape:RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(top: 16.0,bottom: 14.0,left: 14.0),
                            child: Row(
                              children: <Widget>[
                                SvgPicture.asset('assets/svg_icons/back_call.svg'),
                                Expanded(child: Text('ЗАКАЗАТЬ ЗВОНОК',style: TextStyle(color: ColorPallete().brown),textAlign: TextAlign.center,)),
                              ],
                            ),
                          ),
                          onPressed: (){},
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 46.0,),
                ],
              ),
            )
          ],
        ),
      )
    );
  }
}
