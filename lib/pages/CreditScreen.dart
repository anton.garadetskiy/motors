import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:motors/Colors/ColorsPallete.dart';
import 'package:motors/models/CreditItemsModel.dart';
import 'package:motors/models/CreditProgramModel.dart';
import 'package:motors/pages/SliderCreditScreen.dart';
import 'package:motors/widgets/MySeparator.dart';

class CreditScreen extends StatelessWidget {
  CreditScreen({Key key}) : super(key: key);

  List<CreditItem> items = [
    CreditItem(
        'https://mixprogram.ru/upload/000/u1/d/0/toyota-photo-normal.jpg',
        'НОВЫЕ АВТО'),
    CreditItem(
        'https://mixprogram.ru/upload/000/u1/99/00/mazda-photo-normal.jpg',
        'АВТО С ПРОБЕГОМ'),
    CreditItem('https://mixprogram.ru/upload/000/u1/343/4be562f9.jpg',
        'ФИНАНСОВЫЕ УСЛГИ'),
    CreditItem(
        'https://mixprogram.ru/upload/000/u1/83/c1/mitsubishi-photo-normal.jpg',
        'НАШИ УСЛУГИ'),
    CreditItem(
        'https://biz.liga.net/images/general/2008/03/24/200803241448004540.jpg',
        'АКЦИИ КОМПАНИИ'),
    CreditItem(
        'https://drive-my.com/media/com_easysocial/photos/265/17895/2018-mercedes-amg-gt-edition-1-1_thumbnail.jpg',
        'TRADE-IN'),
  ];

  List<ProgrammList> programs = [
    ProgrammList('images/city.png', 'Программа для авто с пробегом',
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem  '),
    ProgrammList('images/city.png', 'Программа для без пробега',
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem  '),
    ProgrammList('images/city.png', 'Программа для авто с пробегом',
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem  ')
  ];

  //Блоки под слайдером
  Widget Items(CreditItem item, BuildContext context) {
    return Expanded(
      child: Container(
        height: 130.0,
        decoration: BoxDecoration(
          color: ColorPallete().red,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Container(
          margin: EdgeInsets.only(bottom: 3.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: Colors.red,
              image: DecorationImage(
                  image: NetworkImage(item.imageUrl), fit: BoxFit.cover)),
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10.0),
                      bottomRight: Radius.circular(10.0)),
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        ColorPallete().itemGradientBegin,
                        ColorPallete().itemGradientEnd
                      ])),
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 12.0, right: 12.0, bottom: 12.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: Text(
                      item.title,
                      style: TextStyle(
                          fontSize: 14.0, color: ColorPallete().white),
                    )),
                    Icon(
                      Icons.arrow_forward,
                      color: ColorPallete().yellow,
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  //Программа для авто с пробегом
  Widget programmItem(ProgrammList program, BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: Image.asset(program.leadingImage)),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 17.25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      program.title,
                      style: TextStyle(
                          fontSize: 14.0, color: ColorPallete().white),
                    ),
                    SizedBox(
                      height: 7.0,
                    ),
                    Text(
                      program.subtitle,
                      style:
                          TextStyle(fontSize: 12.0, color: ColorPallete().gray),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorPallete().backgroundBlack,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SliderApp(),
            SizedBox(
              height: 19.0,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Items(items[0], context),
                      SizedBox(
                        width: 16.48,
                      ),
                      Items(items[1], context),
                    ],
                  ),
                  SizedBox(
                    height: 14.11,
                  ),
                  Row(
                    children: <Widget>[
                      Items(items[2], context),
                      SizedBox(
                        width: 16.48,
                      ),
                      Items(items[3], context),
                    ],
                  ),
                  SizedBox(
                    height: 14.11,
                  ),
                  Row(
                    children: <Widget>[
                      Items(items[4], context),
                      SizedBox(
                        width: 16.48,
                      ),
                      Items(items[5], context),
                    ],
                  ),
                  SizedBox(
                    height: 24.0,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: FlatButton(
                          onPressed: () {},
                          color: Color(0xff85714D),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 15.0),
                            child: Text(
                              'ПОДАТЬ ЗАЯВКУ НА КРЕДИТ',
                              style: TextStyle(
                                  fontSize: 14.0, color: ColorPallete().white),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 24.0,
                  ),
                  programmItem(programs[0], context),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: const MySeparator(
                        color: Color.fromRGBO(182, 188, 201, 0.5)),
                  ),
                  programmItem(programs[1], context),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: const MySeparator(
                        color: Color.fromRGBO(182, 188, 201, 0.5)),
                  ),
                  programmItem(programs[2], context),
                  SizedBox(
                    height: 22.0,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: OutlineButton(
                          borderSide: BorderSide(
                              color: ColorPallete().brown, width: 1.0),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Padding(
                            padding:
                                const EdgeInsets.only(top: 16.0, bottom: 14.0),
                            child: Text(
                              'ВСЕ НОВОСТИ',
                              style: TextStyle(color: ColorPallete().brown),
                            ),
                          ),
                          onPressed: () {},
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 27.0),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
