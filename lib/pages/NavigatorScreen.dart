import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:motors/Colors/ColorsPallete.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:motors/pages/CreditScreen.dart';
import 'package:motors/widgets/Drawer.dart';
import 'package:gradient_bottom_navigation_bar/gradient_bottom_navigation_bar.dart';
import 'package:motors/pages/CreditProgramsScreen.dart';



class StartScreen extends StatefulWidget {
  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  int _current_index = 0;
  final List<Widget> pages = [
    CreditScreen(
      key: PageStorageKey('Page1'),
    ),
    CreditScreen(
      key: PageStorageKey('Page2'),
    ),
    CreditProgramItem(
      key: PageStorageKey('Page3'),
    ),
    CreditScreen(
      key: PageStorageKey('Page4'),
    ),
  ];

  final PageStorageBucket bucket = PageStorageBucket();


  Widget _bottomNavigationBar(int selectedIndex) => GradientBottomNavigationBar(
      backgroundColorStart: ColorPallete().bottomNavBarFirst,
      backgroundColorEnd: ColorPallete().bottomNavBarSecond,
      currentIndex: _current_index,
      onTap: (int index){
        setState(() {
          _current_index = index;
        });
      },
      type: BottomNavigationBarType.fixed,
      items: [
        BottomNavigationBarItem(
          icon: _current_index==0?SvgPicture.asset('assets/svg_icons/bottomNavBar_home_active.svg'):SvgPicture.asset('assets/svg_icons/bottomNavBar_home.svg') ,
          title: Text('Главная',style: TextStyle(fontSize: 12.0,color: _current_index==0?Color(0xffFFB929):Color(0xff666666)),),
        ),
        BottomNavigationBarItem(
          icon: _current_index==1?SvgPicture.asset('assets/svg_icons/bottomNavBar_car_active.svg'):SvgPicture.asset('assets/svg_icons/bottomNavBar_car.svg'),
          title: Text('Автосалон',style: TextStyle(fontSize: 12.0,color: _current_index==1?Color(0xffFFB929):Color(0xff666666)),),
        ),
        BottomNavigationBarItem(
          icon: _current_index==2?SvgPicture.asset('assets/svg_icons/bottomNavBar_wallet_active.svg'):SvgPicture.asset('assets/svg_icons/bottomNavBar_wallet.svg'),
          title: Text('Программы',style: TextStyle(fontSize: 12.0,color: _current_index==2?Color(0xffFFB929):Color(0xff666666)),),
        ),
        BottomNavigationBarItem(
          icon: _current_index==3?SvgPicture.asset('assets/svg_icons/bottomNavBar_people_active.svg'):SvgPicture.asset('assets/svg_icons/bottomNavBar_people.svg'),
          title: Text('Акции',style: TextStyle(fontSize: 12.0,color: _current_index==3?Color(0xffFFB929):Color(0xff666666)),),
        ),
      ]
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorPallete().backgroundBlack,
      appBar: AppBar(
        iconTheme: IconThemeData(color: ColorPallete().brown),
        title: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 12.0),
            child: SvgPicture.asset('assets/svg_icons/astana_motors.svg'),
          ),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: (){},
            icon: Icon(Icons.search),
          ),
          IconButton(
            onPressed: (){},
            icon: Icon(Icons.sort),
          ),
        ],
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  ColorPallete().appBarGradientBegin,
                  ColorPallete().appBarGradientEnd,
                ],
              )
          ),
        ),
      ),
      drawer: DrawerApp(),
      bottomNavigationBar: ClipRRect(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0),topRight: Radius.circular(10.0)),
        child: _bottomNavigationBar(_current_index),
      ),
      body:PageStorage(
        child: pages[_current_index],
        bucket: bucket,
      )
    );
  }
}
