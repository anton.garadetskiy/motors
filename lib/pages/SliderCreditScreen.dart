import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:motors/models/SliderModel.dart';
import 'package:motors/Colors/ColorsPallete.dart';


class SliderApp extends StatefulWidget {
  @override
  _SliderAppState createState() => _SliderAppState();
}

class _SliderAppState extends State<SliderApp> {
  List<SliderItems> slider = [SliderItems('https://www.avtogermes.ru/i/Tucson2018_Polar_White_PYW.png','HYUNDAI TUCSON 2019','98 500'),
    SliderItems('https://d8a6a33f-3369-444b-9b5f-793c13ff0708.selcdn.net/media/common/car_slider_item_v3/tradeins.space/uploads/photo/1577986/29467c8dc61cb70a667805b2159701c8.png?v47','TOYOTA LAND CRUISER','102 500'),
    SliderItems('https://st.motortrend.com/uploads/sites/10/2015/11/2010-subaru-legacy-2.5-i-premium-sedan-angular-front.png','SUBARU LEGACY','65 500'),
    SliderItems('https://www.avtogermes.ru/i/Solaris_2017_Marina_Blue_N4B.png','HYUNDAI SOLARIS','45 500'),
    SliderItems('https://img1.autospot.ru/images/toyota/camry/373703','TOYOTA CAMRY 2019','82 500'),];
  

  int _current = 0;
  CarouselSlider carouselSlider;

  goToPrevious(){
    carouselSlider.previousPage(duration: Duration(milliseconds: 300,),curve: Curves.ease);
  }
  goToNext(){
    carouselSlider.nextPage(duration: Duration(milliseconds: 300,),curve: Curves.ease);
  }
  
  @override
  void initState() {
    super.initState();
    carouselSlider = CarouselSlider(
        height: 146.0,
        viewportFraction:1.0,
        initialPage: 0,
        onPageChanged: (index){
          setState(() {
            _current = index;
          });
        },
        items: slider.map((i){
          return Container(
            child: Image.network(i.imageUrl,fit: BoxFit.cover,),
          );
        }).toList()
    );
  }

  @override
  Widget build(BuildContext context){
    return Container(
      height: 178.0,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.topCenter,
              child: carouselSlider),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Container(
                height: 64.0,
                decoration: BoxDecoration(
                  color: Color(0xff85714D),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap:goToPrevious,
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 7.0),
                          child: Icon(
                            Icons.arrow_back_ios, color: ColorPallete().white,
                            size: 20.0,),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.topLeft,
                        decoration: BoxDecoration(
                          color: ColorPallete().lightBlack,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              bottomLeft: Radius.circular(10.0)),
                        ),

                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 7.0, left: 9.0, bottom: 3.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment
                                .start,
                            children: <Widget>[
                              Text('В кредит', style: TextStyle(
                                  fontSize: 12.0,
                                  letterSpacing: 0.05,
                                  color: ColorPallete().yellow),),
                              SizedBox(height: 3.0,),

                              Text(slider[_current].carModel, style: TextStyle(
                                  fontSize: 14.0,
                                  color: ColorPallete().white),),

                            ],
                          ),

                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          color: Color(0XFF232528),
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(10.0),
                              bottomRight: Radius.circular(10.0)),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 7.0, right: 9.0, bottom: 3.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Text('В месяц от', style: TextStyle(
                                  fontSize: 12.0,
                                  letterSpacing: 0.05,
                                  color: ColorPallete().yellow),),
                              SizedBox(height: 3.0,),
                              Text(slider[_current].carPrice, style: TextStyle(
                                  fontSize: 14.0,
                                  color: ColorPallete().white),),
                              Text('тенге', style: TextStyle(
                                  fontSize: 14.0,
                                  color: ColorPallete().white),)
                            ],
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: goToNext,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Color(0xff85714D),
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(10.0),
                              bottomRight: Radius.circular(10.0)),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 7.0),
                          child: Icon(Icons.arrow_forward_ios,
                            color: ColorPallete().white, size: 20.0,),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );


  }
}
