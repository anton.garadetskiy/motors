

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gradient_bottom_navigation_bar/gradient_bottom_navigation_bar.dart';
import 'package:motors/Colors/ColorsPallete.dart';
import 'package:motors/pages/CreditProgramsScreen.dart';
import 'package:motors/pages/CreditScreen.dart';


class bottomAppBar extends StatefulWidget {
  @override
  _bottomAppBarState createState() => _bottomAppBarState();
}

class _bottomAppBarState extends State<bottomAppBar> {
  @override
  int _current_index = 0;
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(topLeft: Radius.circular(10.0),topRight: Radius.circular(10.0)),
      child: GradientBottomNavigationBar(
          backgroundColorStart: ColorPallete().bottomNavBarFirst,
          backgroundColorEnd: ColorPallete().bottomNavBarSecond,
          currentIndex: _current_index,
          onTap: (int index){
            setState(() {
              _current_index=index;
            });
            switch(index){
              case 0: return CreditScreen();
              case 1:return CreditProgramItem();
              case 2:return CreditProgramItem();
              case 3:return CreditScreen();
              break;
              default: return CreditScreen();
            }
          },
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: _current_index==0?SvgPicture.asset('assets/svg_icons/bottomNavBar_home_active.svg'):SvgPicture.asset('assets/svg_icons/bottomNavBar_home.svg') ,
              title: Text('Главная',style: TextStyle(fontSize: 12.0,color: _current_index==0?Color(0xffFFB929):Color(0xff666666)),),
            ),
            BottomNavigationBarItem(
              icon: _current_index==1?SvgPicture.asset('assets/svg_icons/bottomNavBar_car_active.svg'):SvgPicture.asset('assets/svg_icons/bottomNavBar_car.svg'),
              title: Text('Автосалон',style: TextStyle(fontSize: 12.0,color: _current_index==1?Color(0xffFFB929):Color(0xff666666)),),
            ),
            BottomNavigationBarItem(
              icon: _current_index==2?SvgPicture.asset('assets/svg_icons/bottomNavBar_wallet_active.svg'):SvgPicture.asset('assets/svg_icons/bottomNavBar_wallet.svg'),
              title: Text('Программы',style: TextStyle(fontSize: 12.0,color: _current_index==2?Color(0xffFFB929):Color(0xff666666)),),
            ),
            BottomNavigationBarItem(
              icon: _current_index==3?SvgPicture.asset('assets/svg_icons/bottomNavBar_people_active.svg'):SvgPicture.asset('assets/svg_icons/bottomNavBar_people.svg'),
              title: Text('Акции',style: TextStyle(fontSize: 12.0,color: _current_index==3?Color(0xffFFB929):Color(0xff666666)),),
            ),
          ]
      ),
    );
  }
}


