import 'package:flutter/material.dart';
import 'package:motors/Colors/ColorsPallete.dart';



class Buttons{
  var ButtonWhithBackground = FlatButton(
    onPressed: (){},
    color: ColorPallete().brown,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10.0),
    ),
    child: Padding(
      padding: const EdgeInsets.only(top: 15.0,bottom: 15.0),
      child: Row(
        children: <Widget>[
          Expanded(child: Container(child: Text('КРЕДИТНЫЙ КАЛЬКУЛЯТОР',style: TextStyle(fontSize: 14.0,color: ColorPallete().white),textAlign: TextAlign.center,))),
        ],
      ),
    ),
  );
}