import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:motors/widgets/MySeparator.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:motors/models/DrawerItemsModel.dart';
import 'package:motors/Colors/ColorsPallete.dart';





class DrawerApp extends StatelessWidget {
  List<DrawerItems> item = [DrawerItems('assets/svg_icons/icon_case.svg',' О комании'),
    DrawerItems('assets/svg_icons/icon_hands.svg','Программы кредитования'),
    DrawerItems('assets/svg_icons/icon_persent.svg','Спецпредложения'),
    DrawerItems('assets/svg_icons/icon_car.svg','Выбрать авто/автосалон'),
    DrawerItems('assets/svg_icons/icon_book.svg','Контакты'),
    DrawerItems('assets/svg_icons/icon_circle.svg','Настройки'),
    DrawerItems('assets/svg_icons/icon_exit.svg','Выйти'),
  ];

  Widget DrawerList(DrawerItems items, BuildContext context){
    return ListTile(
      leading: SvgPicture.asset(items.image),
      title: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 18.0,top: 10.0),
              child:  Text(items.title,style: TextStyle(fontSize: 14.0,color:ColorPallete().white)),
            ),
            const MySeparator(color: Color.fromRGBO(255, 255, 255, 0.2)),
          ],
        ),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: ColorPallete().lightBlack,
      ),
      child: Drawer(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage("images/login_bg.png"), fit: BoxFit.cover)
          ),
          child: ListView(
            children: <Widget>[Column(
              children: <Widget>[
                Container(
                  height: 204.0,
                  child: DrawerHeader(
                    child:  Stack(
                        children:<Widget>[
                          Center(
                            child: Column(
                              children: <Widget>[
                                DottedBorder(
                                  padding: EdgeInsets.all(5.0),
                                  borderType: BorderType.Circle,
                                  color: ColorPallete().white,
                                  child: Container(
                                    height: 74.38,
                                    width: 74.38,
                                    child: CircleAvatar(
                                      backgroundColor: Color(0xFFDADADA),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 11.0,),
                                Text('Иван Иванович Иванов',style: TextStyle(
                                    fontSize: 14.0,color: ColorPallete().white
                                ),)
                              ],
                            ),
                          ),
                          Positioned(
                            bottom: 0,
                            right: 0,
                            child: Row(
                              children: <Widget>[
                                Text('Выйти',style: TextStyle(fontSize: 14.0,color: ColorPallete().yellow),),
                                SizedBox(width: 7.0,),
                                Icon(Icons.arrow_forward,color: ColorPallete().yellow,size: 18.0,),
                              ],
                            ),
                          )
                        ]
                    ),
                  ),
                ),


                Column(
                  children:<Widget>[
                    DrawerList(item[0], context),
                    DrawerList(item[1], context),
                    DrawerList(item[2], context),
                    DrawerList(item[3], context),
                    DrawerList(item[4], context),
                    DrawerList(item[5], context),
                    DrawerList(item[6], context),
                  ],
                ),



              ],
            ),],
          ),
        )
      ),
    );
  }
  }



